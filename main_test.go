package main_test

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"bitbucket.org/karrung/chalermrat_srinara/app"
	"bitbucket.org/karrung/chalermrat_srinara/db"
)

func TestMain(m *testing.M) {
	ensureTableUsersExists()

	code := m.Run()

	clearTableUsers()

	os.Exit(code)

}

func ensureTableUsersExists() {
	const tableCreateionQuery = `CREATE TABLE IF NOT EXISTS users (
		id int(11) NOT NULL AUTO_INCREMENT,
		email varchar(254) NOT NULL,
		password varchar(254) DEFAULT NULL,
		first_name varchar(254) DEFAULT NULL,
		last_name varchar(254) DEFAULT NULL,
		address varchar(254) DEFAULT NULL,
		telephone varchar(45) DEFAULT NULL,
		created datetime DEFAULT NULL,
		updated datetime DEFAULT NULL,
		PRIMARY KEY (id),
		UNIQUE KEY email_UNIQUE (email)
	)`
	if _, err := db.DBCon.Exec(tableCreateionQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTableUsers() {
	db.DBCon.Exec("DELETE FROM users")
	db.DBCon.Exec("ALTER TABLE users AUTO_INCREMENT = 1`")
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	app.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func TestHello(t *testing.T) {
	req, _ := http.NewRequest("GET", "/api/v1/hello", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body != `{"result":"hello world"}` {
		t.Errorf("Expected an '{\"result\":\"hello world\"}'. Got %s", body)
	}
}
