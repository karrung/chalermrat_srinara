package app

import (
	"log"
	"net/http"

	ctrl "bitbucket.org/karrung/chalermrat_srinara/controllers"
	"bitbucket.org/karrung/chalermrat_srinara/db"
	mdw "bitbucket.org/karrung/chalermrat_srinara/middleware"
	"bitbucket.org/karrung/chalermrat_srinara/session"
	"github.com/gorilla/mux"
)

var Router *mux.Router

func init() {
	if err := db.ConnectToDatabase(); err != nil {
		log.Fatal(err.Error())
	}

	session.InitCookie()
	initRoutes()
}

func initRoutes() {
	Router = mux.NewRouter()

	Router.Handle("/api/v1/user", http.HandlerFunc(ctrl.CreateUser)).Methods("POST")
	Router.Handle("/api/v1/guser", http.HandlerFunc(ctrl.CreateOrSignInGUser)).Methods("POST")
	Router.Handle("/api/v1/auth", mdw.Auth(http.HandlerFunc(ctrl.Login))).Methods("POST")

	// Forgot Password
	Router.Handle("/api/v1/reset", http.HandlerFunc(ctrl.ForgotPassword)).Methods("POST")
	Router.Handle("/api/v1/reset/{token}", http.HandlerFunc(ctrl.ResetPassword)).Methods("GET")
	Router.Handle("/api/v1/reset/{token}", http.HandlerFunc(ctrl.UpdatePassword)).Methods("PUT")

	// Me
	Router.Handle("/api/v1/me", mdw.Auth(http.HandlerFunc(ctrl.GetMe))).Methods("GET")
	Router.Handle("/api/v1/me", mdw.Auth(http.HandlerFunc(ctrl.UpdateMeProfile))).Methods("PUT")

	// Signout
	Router.Handle("/api/v1/signout", http.HandlerFunc(ctrl.SignOut)).Methods("GET")
}

func Run(addr string) {
	log.Printf("Running on %s\n", addr)
	log.Fatal(http.ListenAndServe(addr, Router))
}
