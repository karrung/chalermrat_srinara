package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"

	h "bitbucket.org/karrung/chalermrat_srinara/handlers"
	"bitbucket.org/karrung/chalermrat_srinara/lib"
	"bitbucket.org/karrung/chalermrat_srinara/models"
	"bitbucket.org/karrung/chalermrat_srinara/session"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	u := new(models.User)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	// Create user
	hash, _ := lib.Hash(u.Password)
	u.Method = "local"
	u.Password = hash
	u.Created = time.Now()
	u.Updated = u.Created
	if err := u.CreateUser(); err != nil {
		h.Error(w, http.StatusInternalServerError, err.Error())
		return
	}

	h.JSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

type GInfo struct {
	Email     string `json:"email"`
	FirstName string `json:"given_name"`
	LastName  string `json:"family_name"`
}

func CreateOrSignInGUser(w http.ResponseWriter, r *http.Request) {
	u := new(models.User)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	// Get user basic info by google id_token
	resp, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + u.GToken)
	if err != nil {
		log.Println(err.Error())
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	g := new(GInfo)
	json.NewDecoder(resp.Body).Decode(&g)
	if err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	// Bind data from google basic info
	u.Method = "google"
	u.Email = g.Email
	u.FirstName = g.FirstName
	u.LastName = g.LastName
	u.Created = time.Now()
	u.Updated = u.Created

	// If user created
	const cookieName = "session"
	if err := u.FindUserByEmail(); err == nil {
		// Set secure cookie
		value := map[string]string{
			"uid":   strconv.Itoa(u.ID),
			"email": u.Email,
		}
		encoded, err := session.SCookie.Encode(cookieName, value)
		if err != nil {
			h.Error(w, http.StatusBadRequest, err.Error())
			return
		}

		ttl := time.Now().Add(60 * time.Minute)
		cookie := &http.Cookie{
			Name:    cookieName,
			Value:   encoded,
			Expires: ttl,
		}
		http.SetCookie(w, cookie)
		h.JSON(w, http.StatusOK, map[string]string{"result": "success"})
		return
	}

	// Create new user
	if err := u.CreateUser(); err != nil {
		h.Error(w, http.StatusInternalServerError, err.Error())
		return
	}

	value := map[string]string{
		"uid":   strconv.Itoa(u.ID),
		"email": u.Email,
	}
	// Set secure cookie
	encoded, err := session.SCookie.Encode(cookieName, value)
	if err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	ttl := time.Now().Add(60 * time.Minute)
	cookie := &http.Cookie{
		Name:    cookieName,
		Value:   encoded,
		Expires: ttl,
	}
	http.SetCookie(w, cookie)
	h.JSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

// Forgot Password
func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	u := new(models.User)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	if err := u.FindUserByEmail(); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	reset := new(models.Reset)
	reset.UID = u.ID
	reset.Token = lib.NewToken(64)
	reset.Created = time.Now()
	if err := reset.CreateReset(); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	if err := lib.SendGmail(u.Email, reset.Token); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	h.JSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

// Reset Password
func ResetPassword(w http.ResponseWriter, r *http.Request) {
	reset := new(models.Reset)
	vars := mux.Vars(r)
	token := vars["token"]
	reset.Token = token

	if err := reset.FindResetByToken(); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	h.JSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

// Reset Password
func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	reset := new(models.Reset)
	vars := mux.Vars(r)
	token := vars["token"]
	reset.Token = token

	// Verify token before update
	if err := reset.FindResetByToken(); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	u := new(models.User)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	if len(u.Password) < 1 {
		h.Error(w, http.StatusBadRequest, "Bad Request")
		return
	}

	u.ID = reset.UID
	hash, _ := lib.Hash(u.Password)
	u.Password = hash
	u.Updated = time.Now()
	if err := u.UpdatePasswordUser(); err != nil {
		h.Error(w, http.StatusInternalServerError, err.Error())
		return
	}

	log.Println(u)
	h.JSON(w, http.StatusCreated, map[string]string{"result": "success"})
}
