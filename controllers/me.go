package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/context"

	h "bitbucket.org/karrung/chalermrat_srinara/handlers"
	// "bitbucket.org/karrung/chalermrat_srinara/lib"
	"bitbucket.org/karrung/chalermrat_srinara/models"
)

func GetMe(w http.ResponseWriter, r *http.Request) {
	id := context.Get(r, "uid")
	if id == nil {
		h.Error(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	u := new(models.User)
	var err error
	u.ID, err = strconv.Atoi(id.(string))
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("Get me:", u.ID)
	if err = u.FindUserByID(); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	// Clear Password before response
	u.Password = ""
	h.JSON(w, http.StatusOK, u)
}

func UpdateMeProfile(w http.ResponseWriter, r *http.Request) {
	uid := context.Get(r, "uid")
	email := context.Get(r, "email")

	if uid == nil || email == nil {
		h.Error(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	u := new(models.User)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	var err error
	u.ID, err = strconv.Atoi(uid.(string))
	if err != nil {
		h.Error(w, http.StatusInternalServerError, err.Error())
		return
	}

	u.Updated = time.Now()
	if err = u.UpdateUser(); err != nil {
		h.Error(w, http.StatusInternalServerError, err.Error())
		return
	}

	h.JSON(w, http.StatusCreated, map[string]string{"result": "success"})
}

func SignOut(w http.ResponseWriter, r *http.Request) {
	log.Println("Sign out")
	const cookieName = "session"
	ttl := time.Now()
	cookie := &http.Cookie{
		Name:    cookieName,
		Expires: ttl,
	}
	http.SetCookie(w, cookie)
	h.JSON(w, http.StatusOK, map[string]string{"result": "success"})
}
