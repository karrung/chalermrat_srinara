package controllers

import (
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"

	h "bitbucket.org/karrung/chalermrat_srinara/handlers"
	// "bitbucket.org/karrung/chalermrat_srinara/lib"
	"bitbucket.org/karrung/chalermrat_srinara/models"
	"bitbucket.org/karrung/chalermrat_srinara/session"
)

func Login(w http.ResponseWriter, r *http.Request) {
	u := new(models.User)
	// Bind data to struct
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	// Check password length before compare
	if len(u.Password) < 1 {
		h.Error(w, http.StatusBadRequest, "Unauthorized")
		return
	}

	// Store source password to compare
	srcPassword := u.Password

	if err := u.FindUserByEmail(); err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	// Compare credential
	b, err := hex.DecodeString(u.Password)
	err = bcrypt.CompareHashAndPassword(b, []byte(srcPassword))
	if err != nil {
		h.Error(w, http.StatusUnauthorized, err.Error())
		return
	}

	value := map[string]string{
		"uid":   strconv.Itoa(u.ID),
		"email": u.Email,
	}

	// Set secure cookie
	const cookieName = "session"
	encoded, err := session.SCookie.Encode(cookieName, value)
	if err != nil {
		h.Error(w, http.StatusBadRequest, err.Error())
		return
	}

	ttl := time.Now().Add(5 * time.Minute)
	cookie := &http.Cookie{
		Name:    cookieName,
		Value:   encoded,
		Expires: ttl,
	}
	http.SetCookie(w, cookie)
	h.JSON(w, http.StatusOK, map[string]string{"result": "success"})
}
