package models

import (
	"time"

	"bitbucket.org/karrung/chalermrat_srinara/db"
)

type User struct {
	ID        int       `json:"-"`
	Method    string    `json:"-"`
	Email     string    `json:"email"`
	Password  string    `json:"password,omitempty"`
	GToken    string    `json:"gtoken,omitempty"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Address   string    `json:"address"`
	Telephone string    `json:"telephone"`
	Created   time.Time `json:"-"`
	Updated   time.Time `json:"-"`
}

func (u *User) UpdateUser() error {
	stmt, err := db.DBCon.Prepare("UPDATE users SET email=?, first_name=?, last_name=?, address=?, telephone=?, updated=? WHERE id=?")
	_, err = stmt.Exec(u.Email, u.FirstName, u.LastName, u.Address, u.Telephone, u.Updated, u.ID)
	if err != nil {
		return err
	}
	return nil
}

func (u *User) UpdatePasswordUser() error {
	stmt, err := db.DBCon.Prepare("UPDATE users SET password=?, updated=? WHERE id=?")
	_, err = stmt.Exec(u.Password, u.Updated, u.ID)
	if err != nil {
		return err
	}
	return nil
}

func (u *User) CreateUser() error {
	stmt, err := db.DBCon.Prepare("INSERT users SET method=?, email=?, password=?, gtoken=?, first_name=?, last_name=?, address=?, telephone=?, created=?, updated=?")
	res, err := stmt.Exec(u.Method, u.Email, u.Password, u.GToken, u.FirstName, u.LastName, u.Address, u.Telephone, u.Created, u.Updated)
	if err != nil {
		return err
	}
	id, _ := res.LastInsertId()
	u.ID = int(id)
	return nil
}

func (u *User) FindUserByEmail() error {
	err := db.DBCon.QueryRow("SELECT id, method, email, password, gtoken, first_name, last_name, address, telephone, created, updated FROM users WHERE email=?", u.Email).Scan(
		&u.ID, &u.Method, &u.Email, &u.Password, &u.GToken, &u.FirstName, &u.LastName, &u.Address, &u.Telephone, &u.Created, &u.Updated)
	if err != nil {
		return err
	}
	return nil
}

func (u *User) FindUserByID() error {
	err := db.DBCon.QueryRow("SELECT id, method, email, password, gtoken, first_name, last_name, address, telephone, created, updated FROM users WHERE id=?", u.ID).Scan(
		&u.ID, &u.Method, &u.Email, &u.Password, &u.GToken, &u.FirstName, &u.LastName, &u.Address, &u.Telephone, &u.Created, &u.Updated)
	if err != nil {
		return err
	}
	return nil
}
