package models

import (
	"time"

	"bitbucket.org/karrung/chalermrat_srinara/db"
)

type Reset struct {
	ID      int       `json:"id"`
	UID     int       `json:"uid"`
	Token   string    `json:"token"`
	Created time.Time `json:"created"`
}

func (r *Reset) CreateReset() error {
	stmt, err := db.DBCon.Prepare("INSERT reset SET uid=?, token=?, created=?")
	res, err := stmt.Exec(r.UID, r.Token, r.Created)
	if err != nil {
		return err
	}
	id, _ := res.LastInsertId()
	r.ID = int(id)
	return nil
}

func (r *Reset) FindResetByToken() error {
	err := db.DBCon.QueryRow("SELECT id, uid, token, created FROM reset WHERE token=?", r.Token).Scan(&r.ID, &r.UID, &r.Token, &r.Created)
	if err != nil {
		return err
	}
	return nil
}
