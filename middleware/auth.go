package middleware

import (
	"log"
	"net/http"

	"bitbucket.org/karrung/chalermrat_srinara/session"
	"github.com/gorilla/context"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		const cookieName = "session"
		cookie, err := r.Cookie(cookieName)
		if err != nil {
			log.Println(err.Error())
			next.ServeHTTP(w, r)
			return
		}

		value := make(map[string]string)
		err = session.SCookie.Decode(cookieName, cookie.Value, &value)
		if err != nil {
			log.Println(err.Error())
		}
		context.Set(r, "uid", value["uid"])
		context.Set(r, "email", value["email"])
		next.ServeHTTP(w, r)
	})
}
