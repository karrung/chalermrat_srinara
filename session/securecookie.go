package session

import (
	"github.com/gorilla/securecookie"
)

var (
	SCookie *securecookie.SecureCookie
)

func InitCookie() {
	hashKey := []byte("secret12345")
	blockKey := []byte("next secret12345")
	SCookie = securecookie.New(hashKey, blockKey)
}
