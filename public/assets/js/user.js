function LoadMe() {
  var email = document.querySelectorAll('#email')[0].value;
  var first_name = document.querySelectorAll('#first_name')[0].value;
  var last_name = document.querySelectorAll('#last_name')[0].value;
  var address = document.querySelectorAll('#address')[0].value;
  var telephone = document.querySelectorAll('#telephone')[0].value;
  axios.get('/api/v1/me', {}, {
    withCredentials: true
  }).then(function (response) {
    document.getElementById("email").innerHTML = response.data.email;
    document.getElementById("first_name").innerHTML = response.data.first_name;
    document.getElementById("last_name").innerHTML = response.data.last_name;
    document.getElementById("address").innerHTML = response.data.address;
    document.getElementById("telephone").innerHTML = response.data.telephone;

    // Input when edit
    document.getElementById("email").value = response.data.email;
    document.getElementById("first_name").value = response.data.first_name;
    document.getElementById("last_name").value = response.data.last_name;
    document.getElementById("address").value = response.data.address;
    document.getElementById("telephone").value = response.data.telephone;
  }).catch(function (error) {
    if (error.response.data.error === 'Unauthorized') {
      return window.location.replace("/login");
    }
    // if (error.response.data.error === 'no result') {
    //   return window.location.replace("/edit");
    // }
  })
}

function SaveProfile() {
  var email = document.querySelectorAll('#email')[0].value;
  var first_name = document.querySelectorAll('#first_name')[0].value;
  var last_name = document.querySelectorAll('#last_name')[0].value;
  var address = document.querySelectorAll('#address')[0].value;
  var telephone = document.querySelectorAll('#telephone')[0].value;

  axios.put('/api/v1/me', {
    email: email,
    first_name: first_name,
    last_name: last_name,
    address: address,
    telephone: telephone
  }, {
    withCredentials: true
  }).then(function (response) {
    console.log(response.data)
    swal(
      'Saved!',
      'You updated profile.',
      'success'
    ).then(function () {
      window.location.replace("/user");
    })
  })
}
