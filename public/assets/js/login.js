function Login() {
  var email = document.querySelectorAll('#email')[0].value;
  var password = document.querySelectorAll('#password')[0].value;
  console.log('Login Clicked', email, password)

  axios.post('/api/v1/auth', {
    email: email,
    password: password
  }, {
    withCredentials: true
  }).then(function (response) {
    swal(
      'Login!',
      'You clicked the button!',
      'success'
    ).then(function () {
      window.location.replace("/user");
    })
  }).catch(function (error) {
    console.log(error.response)
    swal(
      'Something went wrong',
      error.response.data.error,
      'error'
    )
  })
}
