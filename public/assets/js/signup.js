function SignUp() {
    var email = document.querySelectorAll('#email')[0].value;
    var password = document.querySelectorAll('#password')[0].value;
    console.log('SignUp Clicked', email, password)

    axios.post('/api/v1/user', {
        email: email,
        password: password
    }).then(function (response) {
        swal(
            'Registered!',
            'You clicked the button!',
            'success'
        ).then(function () {
            window.location.replace("/");
        })
    }).catch(function (error) {
        console.log(error.response)
        swal(
            'Something went wrong',
            error.response.data.error,
            'error'
        )
    })
}

function signUpCallback(authResult) {
  console.log(authResult)
  // if (authResult['code']) {
  //   console.log(authResult['code'])
  //   axios.post('/api/v1/guser', {
  //     gtoken: authResult['code']
  //   })
  // }
}

$('#gSignupButton').click(function() {
  // signInCallback defined in step 6.
  auth2.grantOfflineAccess().then(signUpCallback);
});

function onGSignUp(gUser) {
  var id_token = gUser.getAuthResponse().id_token;
  // var profile = gUser.getBasicProfile();
  axios.post('/api/v1/guser', {
    gtoken: id_token
  })
  
}
