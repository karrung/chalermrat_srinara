// Local Sign Up
const onLocalSignUp = () => {
  var email = document.querySelectorAll('#email')[0].value;
  var password = document.querySelectorAll('#password')[0].value;
  var first_name = document.querySelectorAll('#first_name')[0].value;
  var last_name = document.querySelectorAll('#last_name')[0].value;
  var address = document.querySelectorAll('#address')[0].value;
  var telephone = document.querySelectorAll('#telephone')[0].value;
  axios.post('/api/v1/user', {
    email, password, first_name, last_name, address, telephone
  }).then(function (response) {
    swal(
      'Registered!',
      '',
      'success'
    ).then(function () {
      window.location.replace("/");
    })
  }).catch(function (error) {
    console.log(error.response)
    swal(
      'Something went wrong',
      error.response.data.error,
      'error'
    )
  })
}

// Local Sign In
const onLocalSignIn = () => {
  var email = document.querySelectorAll('#email')[0].value;
  var password = document.querySelectorAll('#password')[0].value;
  axios.post('/api/v1/auth', {
    email: email,
    password: password
  }, {
    withCredentials: true
  }).then(function (response) {
    swal(
      'Login!',
      'Login success',
      'success'
    ).then(function () {
      window.location.replace("/user");
    })
  }).catch(function (error) {
    console.log(error.response)
    swal(
      'Something went wrong',
      error.response.data.error,
      'error'
    )
  })
}

// Local Sign In
const onGoogleSignIn = (gtoken) => {
  axios.post('/api/v1/guser', {
    gtoken
  }, {
    withCredentials: true
  }).then(res => {
    window.location.replace("/user");
  }).catch(err => {
    swal(
      'Something went wrong',
      err.response.data.error,
      'error'
    )
  })
}

// Reset Password
const onResetPassword = () => {
  var email = document.querySelectorAll('#email')[0].value;
  axios.post('/api/v1/reset', { email}, {withCredentials: true})
    .then(res => {
      swal(
        'Reset password!',
        'Reset password link is send to your email',
        'success'
      )
    }).catch(err => {
      swal(
        'Email not found',
        err.response.data.error,
        'error'
      )
    })
}

// Load Me
const onLoadMe = () => {
  var email = document.querySelectorAll('#email')[0].value;
  var first_name = document.querySelectorAll('#first_name')[0].value;
  var last_name = document.querySelectorAll('#last_name')[0].value;
  var address = document.querySelectorAll('#address')[0].value;
  var telephone = document.querySelectorAll('#telephone')[0].value;
  axios.get('/api/v1/me', {}, {
    withCredentials: true
  }).then(res => {
    // Assign to span
    document.getElementById("email").innerHTML = res.data.email;
    document.getElementById("first_name").innerHTML = res.data.first_name;
    document.getElementById("last_name").innerHTML = res.data.last_name;
    document.getElementById("address").innerHTML = res.data.address;
    document.getElementById("telephone").innerHTML = res.data.telephone;

    // Assign to input when edit
    document.getElementById("email").value = res.data.email;
    document.getElementById("first_name").value = res.data.first_name;
    document.getElementById("last_name").value = res.data.last_name;
    document.getElementById("address").value = res.data.address;
    document.getElementById("telephone").value = res.data.telephone;
  }).catch(err => {
    if (err.response.data.error === 'Unauthorized') {
      return window.location.replace("/");
    }
  })
}

// Save Profile
const onSaveProfile = () => {
  var email = document.querySelectorAll('#email')[0].value;
  var first_name = document.querySelectorAll('#first_name')[0].value;
  var last_name = document.querySelectorAll('#last_name')[0].value;
  var address = document.querySelectorAll('#address')[0].value;
  var telephone = document.querySelectorAll('#telephone')[0].value;

  axios.put('/api/v1/me', {
    email: email,
    first_name: first_name,
    last_name: last_name,
    address: address,
    telephone: telephone
  }, {
    withCredentials: true
  }).then(res => {
    swal(
      'Saved!',
      'You updated profile.',
      'success'
    ).then(() => {
      window.location.replace("/user");
    })
  }).catch(err => {
    swal(
      'Somthing went wrong!',
      'Please try again',
      'error'
    ).then(() => {
      window.location.replace("/");
    })
  })
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// Verify Reset Token
const onLoadReset = () => {
  var token = getParameterByName('token')
  axios.get(`/api/v1/reset/${token}`)
    .then(res => {
      console.log(res.data)
    }).catch(err => {
      swal(
        'Invalid Token',
        'Please try again',
        'error'
      ).then(() => {
        window.location.replace("/");
      })
    })
}

// Reset Password
const onUpdatePassword = () => {
  var password = document.querySelectorAll('#password')[0].value;
  var token = getParameterByName('token')
  axios.put(`/api/v1/reset/${token}`, {password})
    .then(res => {
      swal(
        'Reset Password Success',
        'Please login with new password',
        'success'
      ).then(() => {
        window.location.replace("/");
      })
    }).catch(err => {
      swal(
        'Invalid Token',
        'Please try again',
        'error'
      ).then(() => {
        window.location.replace("/");
      })
    })
}

function onLoad() {
  console.log('onLoad')
  gapi.load('auth2', function() {
    gapi.auth2.init();
  });
}

// Sign Out
function onSignOut() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    axios.get('/api/v1/signout', {withCredentials: true})
      .then(() => {
        window.location.replace("/");
      })
  });
}

