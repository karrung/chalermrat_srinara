package db

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

var (
	DBCon *sql.DB
)

func ConnectToDatabase() error {
	conStr :=
		fmt.Sprintf("%s:%s@tcp(127.0.0.1:3306)/%s?parseTime=true",
			os.Getenv("APP_DB_USERNAME"),
			os.Getenv("APP_DB_PASSWORD"),
			os.Getenv("APP_DB_NAME"))

	var err error
	DBCon, err = sql.Open("mysql", conStr)
	if err != nil {
		return err
	}
	return nil
}
