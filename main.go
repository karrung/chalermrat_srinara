package main

import (
	"bitbucket.org/karrung/chalermrat_srinara/app"
)

func main() {
	app.Run(":9000")
}
