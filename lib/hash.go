package lib

import (
	"encoding/hex"

	"golang.org/x/crypto/bcrypt"
)

// Create hash
func Hash(src string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(src), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(hash), nil
}
