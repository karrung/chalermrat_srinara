package lib

import (
	"crypto/tls"
	"fmt"
	"os"

	gomail "gopkg.in/gomail.v2"
)

func SendGmail(email, token string) error {
	emailAddr := os.Getenv("APP_EMAIL_ADDR")
	emailPwd := os.Getenv("APP_EMAIL_PWD")
	body := fmt.Sprintf("<a href='http://karrung.me/reset?token=%s' target='_blank'>Reset Password</a>", token)
	m := gomail.NewMessage()
	m.SetHeader("From", emailAddr)
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Reset password")
	m.SetBody("text/html", body)

	d := gomail.NewPlainDialer("smtp.gmail.com", 587, emailAddr, emailPwd)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		return err
		panic(err)
	}
	return nil
}
