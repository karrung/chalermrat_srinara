package lib

import (
	"math/rand"
	"time"
)

var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// Getnerate new token, size n
func NewToken(n int) string {
	b := make([]rune, n)
	for i := range b {
		rand.Seed(time.Now().UTC().UnixNano())
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
